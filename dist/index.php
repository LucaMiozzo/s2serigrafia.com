<!DOCTYPE html>
<html lang="en-US" class="no-js scheme_default">

                <!-- Header -->                
                <?php 
                        $page = 'home';
                        include('header.php');?>
                <!-- /Header -->

                <!-- Page content wrap -->
                <div class="page_content_wrap scheme_default">
                    <!-- Content -->
                    <div class="content">
                        <article class="post_item_single page">
                            <div class="post_content">
                                <!-- Revolution slider section -->
                                <?php include('slideshow.php');?>
                                <!-- /Revolution slider section -->

                                <!-- Welcome section -->
                                <div class="copypress-custom-bg-3">
                                    <div class="content_wrap sc_layouts_column_icons_position_left">
                                        <div class="empty_space height_6_3em"></div>
                                        <div class="trx_addons_columns_wrap">
                                            <div class="trx_addons_column-1_2 trx_addons_column_specific sc_layouts_column_icons_position_left">
                                                <div class="empty_space height_3em"></div>
                                                <div class="sc_title sc_title_default">
<!--                                                     <h2 class="sc_item_subtitle sc_title_subtitle sc_align_default sc_item_title_style_default">s2 print</h2>
                                                    <h4 class="sc_item_title sc_title_title sc_align_default sc_item_title_style_default">
                                                        Dal 1986
                                                       </h4> -->
                                                       <img src="images/logo.png" alt="S2 PRiNT" class="home_logo">
                                                </div>
                                                <div class="empty_space height_2em"></div>
                                                
                                                <p>
                                                La ditta <strong>S2 PRINT</strong> nasce nel <strong>1986</strong>, dall'idea di Flavia Dalla Torre e Tiziano Falavigna, inizialmente come serigrafia per trasformarsi nel tempo, sfruttando
                                                le nuove tecnologie, in un’azienda dedicata principalmente alla stampa digitale di grande formato. <br>
                                                </p>
                                                <p>
                                                Nel 2003 si trasferisce nella nuova sede di Mezzocorona dove ha a disposizione una superficie coperta di 800 metri quadrati, in grado di accogliere al coperto anche gli
                                                automezzi di grandi dimensioni, questo ha permesso di offrire al cliente un servizio ancora più completo.<br>
                                                </p>
                                                <p>
                                                In oltre 30 anni di attività l'attenzione è sempre posta a soddisfare tutte le esigenze, come ad esempio la continua ricerca di materiali innovativi da poter offrire
                                                e l'uso di macchinari all'avanguardia con i quali realizzare i prodotti per i clienti.
                                                </p>

                                                <div class="empty_space height_2_9em"></div>
             <!--                                    <div class="sc_item_button sc_button_wrap">
                                                    <a href="#" class="sc_button sc_button_pink sc_button_size_normal sc_button_icon_left">
                                                        <span class="sc_button_text">
                                                            <span class="sc_button_title">Scopri di più</span>
                                                        </span>
                                                    </a>
                                                </div> -->
                                            </div><div class="trx_addons_column-1_2 trx_addons_column_specific sc_layouts_column_icons_position_left">
                                            <div class="empty_space height_4em"></div>
                                            <figure>
                                                <img src="images/photo/IMG_9559.jpg" alt="foto macchina" class="img-thumbnail" />
                                            </figure>
                                        </div>
                                        </div>
                                        <!-- <div class="empty_space height_9_4em"></div> -->
                                    </div>
                                </div>
                                <!-- /Welcome section -->


                                <!-- Projects -->
                                <?php include('projects_home_gallery.php'); ?>
                                <!-- /Projects -->
                                
                                          
                        
                            </div>
                        </article>
                    </div>
                    <!-- /Content -->
                </div>
                <!-- /Page content wrap -->
                
                <!-- Footer -->
                <?php include('footer.php');?>
                <!-- /Footer -->