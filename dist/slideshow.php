
                                <!-- Revolution slider section -->
 
                                <div class="sc_layouts_column_icons_position_left">
                                    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" >
                                        
                                        <!-- START REVOLUTION SLIDER 5.3.0.2 auto mode -->
                                        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
                                            <ul>
                                                <!-- SLIDE  -->
                                                <li data-index="rs-1" data-transition="slideoverup" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="http://placehold.it/100x50" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                    <!-- MAIN IMAGE -->
                                                    <img src="images/slideshow/slide1.jpg" alt="" title="slide1" width="1920" height="700" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                    <!-- LAYERS -->
                                                    <!-- LAYER NR. 1 -->
                                                    <div class="tp-caption tp-resizeme" id="slide-1-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['170','170','170','140']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":600,"speed":700,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="">
                                                    </div>
                                                    <!-- LAYER NR. 2 -->
                                                    <div class="tp-caption tp-resizeme" id="slide-1-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['235','235','235','200']" data-fontsize="['80','80','70','60']" data-lineheight="['80','80','80','70']" data-width="['none','none','none','450']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":950,"speed":700,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                        STAMPA GRANDE FORMATO </div>
                                                    <!-- LAYER NR. 3 -->
                                                    <div class="tp-caption  tp-resizeme" id="slide-1-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['370','370','370','390']" data-fontsize="['20','20','20','23']" data-lineheight="['27','27','27','36']" data-width="['none','none','none','450']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1300,"speed":700,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                        
                                                        <br/> </div>
 
                                                </li>
                                                <!-- SLIDE  -->
                                                <li data-index="rs-2" data-transition="slideoverup" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="http://placehold.it/100x50" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                    <!-- MAIN IMAGE -->
                                                    <img src="images/slideshow/slide2.jpg" alt="" title="slide2" width="1920" height="700" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                    <!-- LAYERS -->
                                                    <!-- LAYER NR. 7 -->
                                                    <div class="tp-caption tp-resizeme" id="slide-2-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['170','170','170','140']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":600,"speed":700,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                         </div>
                                                    <!-- LAYER NR. 8 -->
                                                    <div class="tp-caption tp-resizeme" id="slide-2-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['235','235','225','200']" data-fontsize="['80','80','70','60']" data-lineheight="['80','80','80','70']" data-width="['none','none','701','450']" data-height="none" data-whitespace="['nowrap','nowrap','normal','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":950,"speed":700,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"> 
                                                        TAGLIO E INCISIONE A FRESA</div>
                                                    <!-- LAYER NR. 9 -->
                                                    <div class="tp-caption tp-resizeme" id="slide-2-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['370','370','420','390']" data-fontsize="['20','20','20','23']" data-lineheight="['27','27','27','36']" data-width="['none','none','none','450']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1300,"speed":700,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                        
                                                    </div>


                                                </li>
                                                <!-- SLIDE  -->
                                                <li data-index="rs-3" data-transition="slideoverup" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="http://placehold.it/100x50" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                    <!-- MAIN IMAGE -->
                                                    <img src="images/slideshow/slide3.jpg" alt="" title="slide3" width="1920" height="700" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                    <!-- LAYERS -->
                                                    <!-- LAYER NR. 13 -->
                                                    <div class="tp-caption tp-resizeme" id="slide-3-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['170','170','170','140']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":600,"speed":700,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                         </div>
                                                    <!-- LAYER NR. 14 -->
                                                    <div class="tp-caption tp-resizeme" id="slide-3-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['235','235','225','200']" data-fontsize="['80','80','70','60']" data-lineheight="['80','80','80','70']" data-width="['none','none','701','450']" data-height="none" data-whitespace="['nowrap','nowrap','normal','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":950,"speed":700,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                       DECORAZIONE AUTOMEZZI</div>
                                                    <!-- LAYER NR. 15 -->
                                                    <div class="tp-caption tp-resizeme" id="slide-3-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['370','370','420','370']" data-fontsize="['20','20','20','23']" data-lineheight="['27','27','27','36']" data-width="['none','none','none','451']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1300,"speed":700,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                        
                                                     </div>

                                                </li>



                                                </li>
                                                <!-- SLIDE  -->
                                                <li data-index="rs-4" data-transition="slideoverup" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="http://placehold.it/100x50" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                    <!-- MAIN IMAGE -->
                                                    <img src="images/slideshow/slide4.jpg" alt="" title="slide3" width="1920" height="700" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                    <!-- LAYERS -->
                                                    <!-- LAYER NR. 13 -->
                                                    <div class="tp-caption tp-resizeme" id="new_slide-4-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['170','170','170','140']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":600,"speed":700,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                         </div>
                                                    <!-- LAYER NR. 14 -->
                                                    <div class="tp-caption tp-resizeme" id="new_slide-4-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['235','235','225','200']" data-fontsize="['80','80','70','60']" data-lineheight="['80','80','80','70']" data-width="['none','none','701','450']" data-height="none" data-whitespace="['nowrap','nowrap','normal','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":950,"speed":700,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                        VETROFANIE</div>
                                                    <!-- LAYER NR. 15 -->
                                                    <div class="tp-caption tp-resizeme" id="new_slide-4-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['370','370','420','370']" data-fontsize="['20','20','20','23']" data-lineheight="['27','27','27','36']" data-width="['none','none','none','451']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1300,"speed":700,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                        
                                                     </div>

                                                </li>

                                                <!-- SLIDE  -->
                                                <li data-index="rs-5" data-transition="slideoverup" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="http://placehold.it/100x50" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                    <!-- MAIN IMAGE -->
                                                    <img src="images/slideshow/slide5.jpg" alt="" title="slide4" width="1920" height="700" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                    <!-- LAYERS -->
                                                    <!-- LAYER NR. 13 -->
                                                    <div class="tp-caption tp-resizeme" id="slide-4-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['170','170','170','140']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":600,"speed":700,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                         </div>
                                                    <!-- LAYER NR. 14 -->
                                                    <div class="tp-caption tp-resizeme" id="slide-4-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['235','235','225','200']" data-fontsize="['80','80','70','60']" data-lineheight="['80','80','80','70']" data-width="['none','none','701','450']" data-height="none" data-whitespace="['nowrap','nowrap','normal','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":950,"speed":700,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                    <img src="images/slideshow/slider_logo.png" alt="S2 PRiNT" class="slider_logo"></div>
                                                    <!-- LAYER NR. 15 -->
                                 <!--                    <div class="tp-caption tp-resizeme" id="slide-4-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['370','370','420','370']" data-fontsize="['20','20','20','23']" data-lineheight="['27','27','27','36']" data-width="['none','none','none','451']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1300,"speed":700,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">
                                                        MEZZOCORONA (TN)  <br/>
                                                     </div> -->


                                                    <!-- Contact us button -->
                                                    <div class="tp-caption tp-resizeme" id="slide-4-layer-5" data-x="['center','center','center','center']" 
                                                    data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" 
                                                    data-voffset="['477','477','477','586']" data-fontsize="['14','14','14','16']" 
                                                    data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" 
                                                    data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"contacts.php","delay":""}]' 
                                                    data-responsive_offset="on" 
                                                    data-frames='[{"delay":1650,"speed":700,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},
                                                    {"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},
                                                    {"frame":"hover","speed":"300","ease":"Linear.easeNone","force":true,"to":"o:1;rX:0;rY:0;rZ:0;z:0;"
                                                        ,"style":"c:rgba(255, 255, 255, 1.00);bg:lightgray;"}]' 
                                                    data-textAlign="['inherit','inherit','inherit','inherit']"
                                                    data-paddingtop="[0,0,0,0]" data-paddingright="[40,40,40,40]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[40,40,40,40]">
                                                        Contattaci</div>
  

                                                </li>




                                            </ul>
                                            <div class="tp-bannertimer tp-bottom" style=""></div>
                                        </div>
                                    </div>
                                    <!-- END REVOLUTION SLIDER -->
                                </div>

                                <!-- /Revolution slider section -->
