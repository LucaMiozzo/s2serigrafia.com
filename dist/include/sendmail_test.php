<?php 
global $_REQUEST;
$response = array('error'=>'');
$contact_email = 'info@s2serigrafia.com';

// type
$type = $_REQUEST['type'];	
// parse
parse_str($_POST['data'], $post_data);	
		

$user_name = stripslashes(strip_tags(trim($post_data['username'])));
$user_email = stripslashes(strip_tags(trim($post_data['email'])));
$user_msg =stripslashes(strip_tags(trim( $post_data['message'])));
	


require_once "Mail_test.php";

$from = "S2 Print";
$to = "info@s2serigrafia.com";
$subject = "Contatto da S2 Print";



$senderName = isset( $_POST['senderName'] ) ? preg_replace( "/[^\.\-\' a-zA-Z0-9]/", "", $_POST['senderName'] ) : "";
$senderEmail = isset( $_POST['senderEmail'] ) ? preg_replace( "/[^\.\-\_\@a-zA-Z0-9]/", "", $_POST['senderEmail'] ) : "";
$message = isset( $_POST['message'] ) ? preg_replace( "/(From:|To:|BCC:|CC:|Subject:|Content-Type:)/", "", $_POST['message'] ) : "";
if(isset($_POST['telefono'])) $message= $message. "\nTelefono:" .$_POST['telefono'];



$mail = new EMail;

//Enter your SMTP server (defaults to "127.0.0.1"):
$mail->Server = "smtp.qboxmail.com";    

//Enter your FULL email address:
$mail->Username = 'info@s2serigrafia.com';    

//Enter the password for your email address:
$mail->Password = '';
	
//Enter the email address you wish to send FROM (Name is an optional friendly name):
$mail->SetFrom($senderEmail,$senderName);  

//Enter the email address you wish to send TO (Name is an optional friendly name):
$mail->AddTo($to,"");
//You can add multiple recipients:
//$mail->AddTo("someother2@address.com");

//Enter the Subject of your message:
$mail->Subject = $subject;

//Enter the content of your email message:
$mail->Message = $message;

//Optional extras
$mail->ContentType = "text/html";    // Defaults to "text/plain; charset=iso-8859-1"
//$mail->Headers['X-SomeHeader'] = 'abcde';    // Set some extra headers if required

$success = $mail->Send(); //Send the email.

$headers = "From: " . $senderName . " <" . $senderEmail . ">";
@mail( '', $subject , $message, $headers );

// Return an appropriate response to the browser
if ( isset($_GET["ajax"]) ) {
	echo $success ? "success" : "error";
} else {
?>
<html>
	<head>
	<title>Thanks!</title>
	</head>
	<body>
	<?php if ( $success ) echo "<p>Thanks for sending your message! We'll get back to you shortly.</p>" ?>
	<?php if ( !$success ) echo "<p>There was a problem sending your message. Please try again.</p>" ?>
	<p>Click your browser's Back button to return to the page.</p>
	</body>
</html>
<?php
}
?>