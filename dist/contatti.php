<!DOCTYPE html>
<html lang="en-US" class="no-js scheme_default">

                <!-- Header -->
                <?php 
                $page = "contacts";
                include('header.php'); ?>
                <!-- /Header -->

                <!-- Breadcrumbs -->
<!--                 <div class="breadcrumbs_container sc_layouts_row sc_layouts_row_type_normal sc_layouts_hide_on_frontpage scheme_dark">
                    <div class="sc_layouts_column sc_layouts_column_align_center sc_layouts_column_icons_position_left">
                        <div class="sc_content sc_content_default">
                            <div class="sc_content_container">
                                <div class="sc_layouts_item">
                                    <div class="sc_layouts_title">
                                        <div class="sc_layouts_title_title">
                                            <h2 class="sc_layouts_title_caption location-text">Contatti</h2>
                                        </div>
                                        <div class="sc_layouts_title_breadcrumbs">
                                            <div class="breadcrumbs">
                                                <a class="breadcrumbs_item home" href="index.php">Home</a>
                                                <span class="breadcrumbs_delimiter"></span>
                                                <span class="breadcrumbs_item current">Contatti</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- /Breadcrumbs -->

                <!-- Page content wrap -->
                <div class="page_content_wrap scheme_default">
                    <!-- Content -->
                    <div class="content">
                        <article class="post_item_single">
                            <div class="post_content">
                                <!-- Our Contacts -->
                                <div class="copypress-custom-bg-2">
                                    <div class="content_wrap sc_layouts_column_icons_position_left">
                                        <div class="empty_space height_9_4em"></div>
                                        <div id="sc_form_3" class="sc_form sc_form_detailed simple sc_align_default">
                                            <div class="trx_addons_columns_wrap">
                                                <div class="trx_addons_column-1_2">
                                                    <div class="sc_form_info">
                                                        <h2 class="sc_item_title sc_form_title sc_align_default sc_item_title_style_default">Contattaci</h2>
                                                        <div class="sc_form_info_item sc_form_info_item_address">
                                                            <span class="sc_form_info_icon"></span>
                                                            <span class="sc_form_info_area">
                                                                <span class="sc_form_info_title">Ufficio</span>
                                                                <span class="sc_form_info_data">
                                                                    <span>Via del Teroldego, 1 - Z.I. - 38016 Mezzocorona (TN)</span>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="sc_form_info_item sc_form_info_item_phone">
                                                            <span class="sc_form_info_icon"></span>
                                                            <span class="sc_form_info_area">
                                                                <span class="sc_form_info_title">Telefono</span>
                                                                <span class="sc_form_info_data">
                                                                    <span>0416 604288</span>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="sc_form_info_item sc_form_info_item_email">
                                                            <span class="sc_form_info_icon"></span>
                                                            <span class="sc_form_info_area">
                                                                <span class="sc_form_info_title">Email</span>
                                                                <span class="sc_form_info_data">
                                                                    <a href="mailto:info@s2serigrafia.com">info@s2serigrafia.com</a>
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div><div class="trx_addons_column-1_2">
                                                    <form class="sc_form_form " method="post" action="include/sendmail.php">
                                                        <h2 class="sc_item_title sc_form_title sc_align_default sc_item_title_style_default">Richiedi informazioni</h2>
                                                        <label class="sc_form_field sc_form_field_name required">
                                                            <span class="sc_form_field_wrap">
                                                                <input type="text" name="username" id="username" value="" aria-required="true" placeholder="Nome">
                                                            </span>
                                                        </label>
                                                        <label class="sc_form_field sc_form_field_email required">
                                                            <span class="sc_form_field_wrap">
                                                                <input type="text" name="email" id="email" value="" aria-required="true" placeholder="E-mail">
                                                            </span>
                                                        </label>
                                                        <label class="sc_form_field sc_form_field_message required">
                                                            <span class="sc_form_field_wrap">
                                                                <textarea name="message" id="message" aria-required="true" placeholder="Messaggio"></textarea>
                                                            </span>
                                                        </label>
                                                        <div class="sc_form_field sc_form_field_button">
                                                            <button class="sc_button_dark">Invia messaggio</button>
                                                        </div>
                                                        <div class="trx_addons_message_box sc_form_result"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="empty_space height_9_4em"></div>
                                    </div>
                                </div>
                                <!-- /Our Contacts -->

                           
                                <!-- Google Map -->
                                <div class="sc_layouts_column_icons_position_left">
                                    <div id="sc_googlemap_3_wrap" class="sc_googlemap_wrap">
                                        <div id="sc_googlemap_3" class="sc_googlemap sc_googlemap_default width_100_per height_515" data-zoom="16" data-style="default">
                                            <div id="sc_googlemap_3_1" class="sc_googlemap_marker" data-latlng="" data-address="S2 Serigrafia, Via del Teroldego, 1, 38016 Mezzocorona TN" data-description="" data-title="" data-icon="images/map_marker.png"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Google Map -->

                            </div>
                        </article>
                    </div>
                    <!-- /Content -->
                </div>
                <!-- /Page content wrap -->

                
               <!-- Footer -->
               <?php include('footer.php');?>
                <!-- /Footer -->