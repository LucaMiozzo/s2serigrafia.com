<!DOCTYPE html>
<html lang="en-US" class="no-js scheme_default">

                <!-- Header -->
                <?php 
                $page = "gallery";
                include('header.php'); ?>
                <!-- /Header -->
                <!-- Breadcrumbs -->
                <div class="breadcrumbs_container sc_layouts_row sc_layouts_row_type_normal sc_layouts_hide_on_frontpage scheme_dark">
                    <div class="sc_layouts_column sc_layouts_column_align_center sc_layouts_column_icons_position_left">
                        <div class="sc_content sc_content_default">
                            <div class="sc_content_container">
                                <div class="sc_layouts_item">
                                    <div class="sc_layouts_title">
                                        <div class="sc_layouts_title_title">
                                            <h2 class="sc_layouts_title_caption location-text">Gallery</h2>
                                        </div>
                                        <div class="sc_layouts_title_breadcrumbs">
                                            <div class="breadcrumbs">
                                                <a class="breadcrumbs_item home" href="index.php">Home</a>
                                                <span class="breadcrumbs_delimiter"></span>
                                                <span class="breadcrumbs_item current">Gallery</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Breadcrumbs -->


                <!-- Page content wrap -->
                <div class="page_content_wrap scheme_default copypress-custom-bg-1">
                    <div class="content_wrap">
                        <!-- Content -->
                        <div class="content">
                            <article class="post_item_single page">
                                <div class="post_content">
                                    <div class="empty_space height_6_67em"></div>
                                    <!-- THE ESSENTIAL GRID -->
                                    <article class="myportfolio-container minimal-light" id="esg-grid-4-1-wrap">
                                        <div id="esg-grid-4-1" class="esg-grid">
                                            <article class="esg-filters esg-singlefilters">
                                                <!-- THE FILTER BUTTONS -->
                                                <div class="esg-filter-wrapper esg-fgc-4">
                                                    <div class="esg-filterbutton selected esg-allfilter" data-filter="filterall" data-fid="-1">
                                                        <span>Tutto</span>
                                                    </div>
                                                    <div class="esg-filterbutton" data-fid="2" data-filter="filter-automezzi">
                                                        <span>Automezzi</span>
                                                        <span class="esg-filter-checked">
                                                            <i class="eg-icon-ok-1"></i>
                                                        </span>
                                                    </div>
                                                    <div class="esg-filterbutton" data-fid="3" data-filter="filter-stand">
                                                        <span>Stand Fiere</span>
                                                        <span class="esg-filter-checked">
                                                            <i class="eg-icon-ok-1"></i>
                                                        </span>
                                                    </div>
                                                    <div class="esg-filterbutton" data-fid="4" data-filter="filter-custom-prints">
                                                        <span>Stampe personalizzate</span>
                                                        <span class="esg-filter-checked">
                                                            <i class="eg-icon-ok-1"></i>
                                                        </span>
                                                    </div>
                                                    <div class="esg-filterbutton" data-fid="5" data-filter="filter-varie">
                                                        <span>Varie</span>
                                                        <span class="esg-filter-checked">
                                                            <i class="eg-icon-ok-1"></i>
                                                        </span>
                                                    </div>
                                                    <div class="eg-clearfix"></div>
                                                </div>
                                            </article>
                                            <div class="esg-clear-no-height"></div>
                                            <ul>
                      
                                                <?php 
                                                /* Loading automezzi */
                                                $url_automezzi = "images/automezzi/";
                                                $url_stand = "images/stand/";
                                                $url_prints = "images/prints/";
                                                $url_varie= "images/varie/";

                                                $images_arr = array();
                                                $images_arr[0] = array("url"=>$url_automezzi."IMAG0250.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "2", "y" => "2");
                                                $images_arr[1] = array("url"=>$url_automezzi."IMG_2307.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[2] = array("url"=>$url_automezzi."P3260010.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[3] = array("url"=>$url_automezzi."IMAG0249.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[4] = array("url"=>$url_automezzi."IMG_4458.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[5] = array("url"=>$url_automezzi."IMG_5321.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[6] = array("url"=>$url_automezzi."IMG_5898.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[7] = array("url"=>$url_automezzi."IMG_8879.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "2", "y" => "2");
                                                $images_arr[8] = array("url"=>$url_automezzi."IMG_9807.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[9] = array("url"=>$url_automezzi."IMAG0080.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[10] = array("url"=>$url_automezzi."IMG_4328.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[11] = array("url"=>$url_automezzi."IMG_3408.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[12] = array("url"=>$url_automezzi."IMG_3377.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[13] = array("url"=>$url_automezzi."IMAG0190.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[14] = array("url"=>$url_automezzi."IMG_1992.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[15] = array("url"=>$url_automezzi."IMG_1466.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[16] = array("url"=>$url_automezzi."IMAG1214.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "2", "y" => "1");
                                                $images_arr[17] = array("url"=>$url_automezzi."IMAG1202.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[18] = array("url"=>$url_automezzi."IMG_3988.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[19] = array("url"=>$url_automezzi."IMAG0999.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[20] = array("url"=>$url_automezzi."IMAG0953.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[21] = array("url"=>$url_automezzi."IMAG0950.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[22] = array("url"=>$url_automezzi."IMAG0948.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[23] = array("url"=>$url_automezzi."IMAG0947.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[24] = array("url"=>$url_automezzi."IMAG0749.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "2", "y" => "2");
                                                $images_arr[25] = array("url"=>$url_automezzi."IMAG0714.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[26] = array("url"=>$url_automezzi."IMAG0625.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "2", "y" => "2");
                                                $images_arr[27] = array("url"=>$url_automezzi."IMAG0275.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[28] = array("url"=>$url_automezzi."IMAG0123.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[29] = array("url"=>$url_automezzi."IMAG1122_BURST002_COVER.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[80] = array("url"=>$url_automezzi."IMG_4327.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                

                                                
                                                $images_arr[30] = array("url"=>$url_stand."IMAG0308.jpg", "title"=>"", "filter" => "filter-stand", "x" => "2", "y" => "1");
                                                $images_arr[31] = array("url"=>$url_stand."IMG_0813.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                $images_arr[32] = array("url"=>$url_stand."IMG_3208.jpg", "title"=>"", "filter" => "filter-stand", "x" => "2", "y" => "2");
                                                $images_arr[33] = array("url"=>$url_stand."IMG_9174.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                $images_arr[34] = array("url"=>$url_stand."IMG_9377.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                $images_arr[35] = array("url"=>$url_stand."IMG_9753.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                $images_arr[36] = array("url"=>$url_stand."IMG_9755.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                


                                                $images_arr[37] = array("url"=>$url_prints."IMAG1247.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "2", "y" => "2");
                                                $images_arr[38] = array("url"=>$url_prints."IMAG1248.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "2", "y" => "1");
                                                $images_arr[39] = array("url"=>$url_prints."IMAG1304.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "2", "y" => "2");
                                                $images_arr[40] = array("url"=>$url_prints."IMG_0211.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "1", "y" => "2");
                                                $images_arr[41] = array("url"=>$url_prints."IMG_0225.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "1", "y" => "1");
                                                $images_arr[42] = array("url"=>$url_prints."IMG_0226.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "1", "y" => "1");
                                                $images_arr[43] = array("url"=>$url_prints."IMG_0974.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "1", "y" => "1");
                                                $images_arr[44] = array("url"=>$url_prints."IMG_1266.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "2", "y" => "2");
                                                $images_arr[45] = array("url"=>$url_prints."IMG_3242.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "1", "y" => "1");
                                                $images_arr[46] = array("url"=>$url_prints."IMG_3251.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "1", "y" => "1");
                                                $images_arr[47] = array("url"=>$url_prints."IMG_5539.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "1", "y" => "1");
                                                $images_arr[48] = array("url"=>$url_prints."IMG_7487.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "2", "y" => "2");
                                                $images_arr[49] = array("url"=>$url_prints."IMG_9148.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "1", "y" => "1");
                                                $images_arr[50] = array("url"=>$url_prints."IMG_9151.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "2", "y" => "1");
                                                $images_arr[51] = array("url"=>$url_prints."IMG_9762.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "1", "y" => "2");
                                                $images_arr[52] = array("url"=>$url_prints."Pannello_Danta.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "1", "y" => "1");
                                                $images_arr[53] = array("url"=>$url_prints."Tabella_Zoppe.jpg", "title"=>"", "filter" => "filter-custom-prints", "x" => "1", "y" => "2");


                                                $images_arr[54] = array("url"=>$url_varie."IMAG0395.jpg", "title"=>"", "filter" => "filter-varie", "x" => "2", "y" => "2");
                                                $images_arr[55] = array("url"=>$url_varie."IMAG0409.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[56] = array("url"=>$url_varie."IMAG0525.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[57] = array("url"=>$url_varie."IMAG0603.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[58] = array("url"=>$url_varie."IMAG0684.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "2");
                                                $images_arr[59] = array("url"=>$url_varie."IMAG0985.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "2");
                                                $images_arr[60] = array("url"=>$url_varie."IMG_0239.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[61] = array("url"=>$url_varie."IMG_0591.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "2");
                                                $images_arr[62] = array("url"=>$url_varie."IMG_0693.jpg", "title"=>"", "filter" => "filter-varie", "x" => "2", "y" => "2");
                                                $images_arr[63] = array("url"=>$url_varie."IMG_1090.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "2");
                                                $images_arr[64] = array("url"=>$url_varie."IMG_2812.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[65] = array("url"=>$url_varie."IMG_3138.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[66] = array("url"=>$url_varie."IMG_3140.jpg", "title"=>"", "filter" => "filter-varie", "x" => "2", "y" => "2");
                                                $images_arr[67] = array("url"=>$url_varie."IMG_3248.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[68] = array("url"=>$url_varie."IMG_3508.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[69] = array("url"=>$url_varie."IMG_3510.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[70] = array("url"=>$url_varie."IMG_3516.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[71] = array("url"=>$url_varie."IMG_4988.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[72] = array("url"=>$url_varie."IMG_5040.jpg", "title"=>"", "filter" => "filter-varie", "x" => "2", "y" => "2");
                                                $images_arr[73] = array("url"=>$url_varie."IMG_5718.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "2");
                                                $images_arr[74] = array("url"=>$url_varie."IMG_9657.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[75] = array("url"=>$url_varie."IMG_9769.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[76] = array("url"=>$url_varie."IMG_9774.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[77] = array("url"=>$url_varie."IMG_9800.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[78] = array("url"=>$url_varie."IMG_9815.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                                                $images_arr[79] = array("url"=>$url_varie."IMG_9822.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                      

                                                shuffle($images_arr);

                                            
                                                foreach($images_arr as $img){
                                                    echo '
                                                    
                                                    <!-- Grid Item -->
                                                    <li class="filterall '.$img['filter'].' filter-business-cards filter-posters eg-washington-copypress-wrapper" data-cobblesw="'.$img['x'].'" data-cobblesh="'.$img['y'].'">
                                                        <a  href="'.$img['url'].'" alt="'.$img['title'].'" data-lightbox="gallery_">
                                                        <div class="esg-media-cover-wrapper">
                                                            <div class="esg-entry-media">
                                                                <img src="'.$img['url'].'" alt="'.$img['title'].'">
                                                            </div>
                                                            <div class="esg-entry-cover esg-fade" data-delay="0">
                                                                <div class="esg-overlay esg-slideright eg-washington-copypress-container" data-delay="0"></div>
                                                                <div class="esg-absolute eg-washington-copypress-element-1-a esg-fade" data-delay="0.4">
                                                                    <!-- <a class="eg-washington-copypress-element-1" href="'.$img['url'].'" alt="'.$img['title'].'" data-lightbox="gallery_'.$key.'_2">
                                                                    <i class="eg-icon-right-open-1"></i>
                                                                    </a> -->
                                                                </div>
                                                                <div class="esg-center eg-washington-copypress-element-8 esg-none esg-clear"></div>
                                                                <div class="esg-center eg-washington-copypress-element-9 esg-none esg-clear"></div>
                                                                <!-- <div class="esg-bottom eg-washington-copypress-element-3 esg-slideright" data-delay="0.3">"'.$img['title'].'"</div> -->
                                                            </div>
                                                        </div>
                                                        </a>
                                                    </li>
                                                    <!-- /Grid Item -->
                                                    
                                                    ';
                                                }


                                                ?>
                                            </ul>
                                        </div>
                                    </article>
                                    <div class="clear"></div>
                                    <div class="empty_space height_6_67em"></div>
                                </div>
                            </article>
                        </div>
                        <!-- /Content -->
                    </div>
                </div>
                <!-- /Page content wrap -->
                <!-- Footer -->
                <?php include('footer.php'); ?>
                <!-- /Foooter -->