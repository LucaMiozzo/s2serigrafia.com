<!DOCTYPE html>
<html lang="en-US" class="no-js scheme_default">

                <!-- Header -->
                <?php 
                $page = "gallery";
                $sub_page = "automezzi";
                include('header.php'); ?>
                <!-- /Header -->
                <!-- Breadcrumbs -->
<!--                 <div class="breadcrumbs_container sc_layouts_row sc_layouts_row_type_normal sc_layouts_hide_on_frontpage scheme_dark">
                    <div class="sc_layouts_column sc_layouts_column_align_center sc_layouts_column_icons_position_left">
                        <div class="sc_content sc_content_default">
                            <div class="sc_content_container">
                                <div class="sc_layouts_item">
                                    <div class="sc_layouts_title">
                                        <div class="sc_layouts_title_title">
                                            <h2 class="sc_layouts_title_caption location-text">Gallery</h2>
                                        </div>
                                        <div class="sc_layouts_title_breadcrumbs">
                                            <div class="breadcrumbs">
                                                <a class="breadcrumbs_item home" href="index.php">Home</a>
                                                <span class="breadcrumbs_delimiter"></span>
                                                <span class="breadcrumbs_item current">Gallery</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- /Breadcrumbs -->


                <!-- Page content wrap -->
                <div class="page_content_wrap scheme_default copypress-custom-bg-1">
                    <div class="content_wrap">
                        <!-- Content -->
                        <div class="content">
                            <article class="post_item_single page">
                                <div class="post_content">
                                    <div class="empty_space height_6_67em"></div>
                                    <!-- THE ESSENTIAL GRID -->
                                    <article class="myportfolio-container minimal-light" id="esg-grid-4-1-wrap">
                                        <div id="esg-grid-4-1" class="esg-grid">
                                            <!-- THE FILTER BUTTONS -->
                                            <article class="esg-filters esg-singlefilters">                                                
                                                <h2>AUTOMEZZI</h2>
                                            </article>
                                            <div class="esg-clear-no-height"></div>
                                            <ul>
                      
                                                <?php 
                                                /* Loading automezzi */
                                                $url_automezzi = "images/automezzi/";

                                                $images_arr = array();
                                                $images_arr[1] = array("url"=>$url_automezzi."IMAG0076_2.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "2");
                                                $images_arr[2] = array("url"=>$url_automezzi."IMAG0247.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[3] = array("url"=>$url_automezzi."IMAG0892.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[4] = array("url"=>$url_automezzi."IMAG0893.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[5] = array("url"=>$url_automezzi."IMAG0894.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[6] = array("url"=>$url_automezzi."IMAG0895.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[7] = array("url"=>$url_automezzi."IMAG1214.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "2", "y" => "1");
                                                $images_arr[8] = array("url"=>$url_automezzi."IMAG1216.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[9] = array("url"=>$url_automezzi."IMAG1261.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[10] = array("url"=>$url_automezzi."IMAG1262.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[11] = array("url"=>$url_automezzi."IMAG1409.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[12] = array("url"=>$url_automezzi."IMG_1340.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[13] = array("url"=>$url_automezzi."IMG_1349.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[14] = array("url"=>$url_automezzi."IMG_1353.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[15] = array("url"=>$url_automezzi."IMG_1466.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");

                                                $images_arr[16] = array("url"=>$url_automezzi."IMAG0439.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[17] = array("url"=>$url_automezzi."IMAG0473.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[18] = array("url"=>$url_automezzi."IMAG1425.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[19] = array("url"=>$url_automezzi."IMAG1499.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[20] = array("url"=>$url_automezzi."IMAG1514.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[21] = array("url"=>$url_automezzi."IMAG0440.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[22] = array("url"=>$url_automezzi."IMAG0521.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "2", "y" => "1");
                                                $images_arr[23] = array("url"=>$url_automezzi."IMAG1428.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[24] = array("url"=>$url_automezzi."IMAG1500.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[25] = array("url"=>$url_automezzi."IMAG1515.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "2");
                                                $images_arr[26] = array("url"=>$url_automezzi."IMAG0442.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[27] = array("url"=>$url_automezzi."IMAG0522.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[28] = array("url"=>$url_automezzi."IMAG1496.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "2", "y" => "1");
                                                $images_arr[29] = array("url"=>$url_automezzi."IMAG1501.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[30] = array("url"=>$url_automezzi."IMAG0443.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[31] = array("url"=>$url_automezzi."IMAG0524.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[32] = array("url"=>$url_automezzi."IMAG1497.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[33] = array("url"=>$url_automezzi."IMAG1502.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
    
                                                $images_arr[34] = array("url"=>$url_automezzi."IMAG0075.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[35] = array("url"=>$url_automezzi."IMAG0122.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "2", "y" => "1");
                                                $images_arr[36] = array("url"=>$url_automezzi."IMAG0158.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[37] = array("url"=>$url_automezzi."IMAG0190.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[38] = array("url"=>$url_automezzi."IMAG0275.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "2", "y" => "1");
                                                $images_arr[39] = array("url"=>$url_automezzi."IMAG0953.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[40] = array("url"=>$url_automezzi."IMG_5094.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[41] = array("url"=>$url_automezzi."IMG_5100.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "2", "y" => "1");
                                                $images_arr[42] = array("url"=>$url_automezzi."IMAG0210.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                                                $images_arr[43] = array("url"=>$url_automezzi."IMAG0277.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");

                                                shuffle($images_arr);

                                             $cont = 0;
                                                foreach($images_arr as $key=>$img){ 
                                                    echo '
                                                    
                                                    <!-- Grid Item -->
                                                    <li class="filterall '.$img['filter'].' filter-business-cards filter-posters eg-washington-copypress-wrapper" data-cobblesw="'.$img['x'].'" data-cobblesh="'.$img['y'].'">
                                                    <a  href="'.$img['url'].'" alt="'.$img['title'].'" data-lightbox="gallery_">
                                                        <div class="esg-media-cover-wrapper">
                                                            <div class="esg-entry-media">
                                                                <img src="'.$img['url'].'" alt="'.$img['title'].'">
                                                            </div>
                                                            <div class="esg-entry-cover esg-fade" data-delay="0">
                                                                <div class="esg-overlay esg-slideright eg-washington-copypress-container" data-delay="0"></div>
                                                                <div class="esg-absolute eg-washington-copypress-element-1-a esg-fade" data-delay="0.4">
                                                                   <!-- <a class="eg-washington-copypress-element-1" href="'.$img['url'].'" alt="'.$img['title'].'" data-lightbox="gallery_'.$key.'_2">
                                                                        <i class="eg-icon-right-open-1"></i>
                                                                    </a> -->
                                                                </div>
                                                                <div class="esg-center eg-washington-copypress-element-8 esg-none esg-clear"></div>
                                                                <div class="esg-center eg-washington-copypress-element-9 esg-none esg-clear"></div>
                                                                <!-- <div class="esg-bottom eg-washington-copypress-element-3 esg-slideright" data-delay="0.3">"'.$img['title'].'"</div> -->
                                                            </div>
                                                        </div>
                                                        </a>
                                                    </li>
                                                    <!-- /Grid Item -->
                                                    
                                                    ';
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </article>
                                    <div class="clear"></div>
                                    <div class="empty_space height_6_67em"></div>
                                    <div class="empty_space height_6_67em"></div>
                                </div>
                            </article>
                        </div>
                        <!-- /Content -->
                    </div>
                </div>
                <!-- /Page content wrap -->
                <!-- Footer -->
                <?php include('footer.php'); ?>
                <!-- /Foooter -->