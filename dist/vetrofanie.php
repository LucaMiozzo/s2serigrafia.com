<!DOCTYPE html>
<html lang="en-US" class="no-js scheme_default">

                <!-- Header -->
                <?php 
                $page = "gallery";
                $sub_page = "vetrofanie";
                include('header.php'); ?>
                <!-- /Header -->
                <!-- Breadcrumbs -->
<!--                 <div class="breadcrumbs_container sc_layouts_row sc_layouts_row_type_normal sc_layouts_hide_on_frontpage scheme_dark">
                    <div class="sc_layouts_column sc_layouts_column_align_center sc_layouts_column_icons_position_left">
                        <div class="sc_content sc_content_default">
                            <div class="sc_content_container">
                                <div class="sc_layouts_item">
                                    <div class="sc_layouts_title">
                                        <div class="sc_layouts_title_title">
                                            <h2 class="sc_layouts_title_caption location-text">Gallery</h2>
                                        </div>
                                        <div class="sc_layouts_title_breadcrumbs">
                                            <div class="breadcrumbs">
                                                <a class="breadcrumbs_item home" href="index.php">Home</a>
                                                <span class="breadcrumbs_delimiter"></span>
                                                <span class="breadcrumbs_item current">Gallery</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- /Breadcrumbs -->


                <!-- Page content wrap -->
                <div class="page_content_wrap scheme_default copypress-custom-bg-1">
                    <div class="content_wrap">
                        <!-- Content -->
                        <div class="content">
                            <article class="post_item_single page">
                                <div class="post_content">
                                    <div class="empty_space height_6_67em"></div>
                                    <!-- THE ESSENTIAL GRID -->
                                    <article class="myportfolio-container minimal-light" id="esg-grid-4-1-wrap">
                                        <div id="esg-grid-4-1" class="esg-grid">
                                            <!-- THE FILTER BUTTONS -->
                                            <article class="esg-filters esg-singlefilters">                                                
                                                <h2>VETROFANIE</h2>
                                            </article>
                                            <div class="esg-clear-no-height"></div>
                                            <ul>
                      
                                                <?php 
                                                /* Loading imgs */                                              
                                                $url_glass = "images/vetrofanie/";
               

                                                $images_arr = array();
                                                
                                                $images_arr[0] = array("url"=>$url_glass."IMAG0203.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                $images_arr[1] = array("url"=>$url_glass."IMAG0234.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                $images_arr[2] = array("url"=>$url_glass."IMAG0395.jpg", "title"=>"", "filter" => "filter-stand", "x" => "2", "y" => "1");
                                                $images_arr[3] = array("url"=>$url_glass."IMAG0447.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                $images_arr[4] = array("url"=>$url_glass."IMAG0845.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                $images_arr[5] = array("url"=>$url_glass."IMAG0985.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "2");
                                                $images_arr[6] = array("url"=>$url_glass."IMG_0469.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                $images_arr[7] = array("url"=>$url_glass."IMG_5020.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                $images_arr[8] = array("url"=>$url_glass."IMG_5038.jpg", "title"=>"", "filter" => "filter-stand", "x" => "2", "y" => "1");
                                                $images_arr[9] = array("url"=>$url_glass."IMG_5146.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
                                                $images_arr[10] = array("url"=>$url_glass."IMG_5160.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");
      
                                                $images_arr[11] = array("url"=>$url_glass."IMG_0122.jpg", "title"=>"", "filter" => "filter-stand", "x" => "1", "y" => "1");


                                                shuffle($images_arr);

                                            
                                                foreach($images_arr as $img){
                                                    echo '
                                                    
                                                    <!-- Grid Item -->
                                                    <li class="filterall '.$img['filter'].' filter-business-cards filter-posters eg-washington-copypress-wrapper" data-cobblesw="'.$img['x'].'" data-cobblesh="'.$img['y'].'">
                                                        <a  href="'.$img['url'].'" alt="'.$img['title'].'" data-lightbox="gallery_">    
                                                        <div class="esg-media-cover-wrapper">
                                                            <div class="esg-entry-media">
                                                                <img src="'.$img['url'].'" alt="'.$img['title'].'">
                                                            </div>
                                                            <div class="esg-entry-cover esg-fade" data-delay="0">
                                                                <div class="esg-overlay esg-slideright eg-washington-copypress-container" data-delay="0"></div>
                                                                <div class="esg-absolute eg-washington-copypress-element-1-a esg-fade" data-delay="0.4">
                                                                    <!-- <a class="eg-washington-copypress-element-1" href="'.$img['url'].'" alt="'.$img['title'].'" data-lightbox="gallery_'.$key.'_2">
                                                                    <i class="eg-icon-right-open-1"></i>
                                                                    </a> -->
                                                                </div>
                                                                <div class="esg-center eg-washington-copypress-element-8 esg-none esg-clear"></div>
                                                                <div class="esg-center eg-washington-copypress-element-9 esg-none esg-clear"></div>
                                                                <!-- <div class="esg-bottom eg-washington-copypress-element-3 esg-slideright" data-delay="0.3">"'.$img['title'].'"</div> -->
                                                            </div>
                                                        </div>
                                                        </a>
                                                    </li>
                                                    <!-- /Grid Item -->
                                                    
                                                    ';
                                                }


                                                ?>
                                            </ul>
                                        </div>
                                    </article>
                                    <div class="clear"></div>
                                    <div class="empty_space height_6_67em"></div>
                                    <div class="empty_space height_6_67em"></div>
                                </div>
                            </article>
                        </div>
                        <!-- /Content -->
                    </div>
                </div>
                <!-- /Page content wrap -->
                <!-- Footer -->
                <?php include('footer.php'); ?>
                <!-- /Foooter -->