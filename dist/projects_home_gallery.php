<!-- Projects -->
<div class="copypress-custom-bg-1">
    <div class="sc_layouts_column_icons_position_left">
        <div class="empty_space height_9_4em"></div>
        <div class="sc_title sc_title_default">
            <h6 class="sc_item_subtitle sc_title_subtitle sc_align_center sc_item_title_style_large">Lavori Recenti</h6>
            <h2 class="sc_item_title sc_title_title sc_align_center sc_item_title_style_large">Galleria</h2>
        </div>
        <div class="empty_space height_3em"></div>
        <!-- THE ESSENTIAL GRID 2.1.0.2 -->
        <article class="myportfolio-container minimal-light" id="esg-grid-1-1-wrap">
            <div id="esg-grid-1-1" class="esg-grid">
                <article class="esg-filters esg-singlefilters text_align_center margin_bottom_60">                    
                    <div class="esg-filter-wrapper esg-fgc-1">
                        <div class="esg-filterbutton selected esg-allfilter" data-filter="filterall" data-fid="-1">
                            <span>Tutto</span>
                        </div>
                        <div class="esg-filterbutton" data-fid="2" data-filter="filter-automezzi">
                            <span>Automezzi</span>
                            <span class="esg-filter-checked">
                                <i class="eg-icon-ok-1"></i>
                            </span>
                        </div>
                        <div class="esg-filterbutton" data-fid="3" data-filter="filter-glass">
                            <span>Vetrofanie</span>
                            <span class="esg-filter-checked">
                                <i class="eg-icon-ok-1"></i>
                            </span>
                        </div>
                        <div class="esg-filterbutton" data-fid="5" data-filter="filter-prints">
                            <span>Grande formato</span>
                            <span class="esg-filter-checked">
                                <i class="eg-icon-ok-1"></i>
                            </span>
                        </div>
                        <div class="esg-filterbutton" data-fid="6" data-filter="filter-varie">
                            <span>Altro</span>
                            <span class="esg-filter-checked">
                                <i class="eg-icon-ok-1"></i>
                            </span>
                        </div>
                        <div class="eg-clearfix"></div>
                    </div>
                </article>
                <div class="esg-clear-no-height"></div>



                <ul>

                    <?php 
                    /* Loading imgs */
                    $url_automezzi = "images/automezzi/";
                    $url_glass = "images/vetrofanie/";
                    $url_prints = "images/grande_formato/";
                    $url_altro= "images/altro/";

                    $images_arr = array();

                    $images_arr[0] = array("url"=>$url_automezzi."IMG_5100.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                    $images_arr[1] = array("url"=>$url_automezzi."IMAG1496.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                    $images_arr[2] = array("url"=>$url_automezzi."IMAG0275.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");
                    $images_arr[3] = array("url"=>$url_automezzi."IMAG0075.jpg", "title"=>"", "filter" => "filter-automezzi", "x" => "1", "y" => "1");

                    $images_arr[4] = array("url"=>$url_glass."IMAG0203.jpg", "title"=>"", "filter" => "filter-glass", "x" => "1", "y" => "1");
                    $images_arr[5] = array("url"=>$url_glass."IMAG0234.jpg", "title"=>"", "filter" => "filter-glass", "x" => "1", "y" => "1");
                    $images_arr[6] = array("url"=>$url_glass."IMAG0395.jpg", "title"=>"", "filter" => "filter-glass", "x" => "1", "y" => "1");
                    $images_arr[7] = array("url"=>$url_glass."IMG_0122.jpg", "title"=>"", "filter" => "filter-glass", "x" => "1", "y" => "1");

                    $images_arr[8] = array("url"=>$url_prints."IMAG0068_2.jpg", "title"=>"", "filter" => "filter-prints", "x" => "1", "y" => "1");
                    $images_arr[9] = array("url"=>$url_prints."IMAG0135.jpg", "title"=>"", "filter" => "filter-prints", "x" => "1", "y" => "1");
                    $images_arr[10] = array("url"=>$url_prints."IMAG1247.jpg", "title"=>"", "filter" => "filter-prints", "x" => "1", "y" => "1");
                    $images_arr[11] = array("url"=>$url_prints."IMG_1175.jpg", "title"=>"", "filter" => "filter-prints", "x" => "1", "y" => "1");

                    $images_arr[12] = array("url"=>$url_altro."IMAG1365.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                    $images_arr[13] = array("url"=>$url_altro."IMAG1406.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                    $images_arr[14] = array("url"=>$url_altro."IMG_4985.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");
                    $images_arr[15] = array("url"=>$url_altro."IMG_20190201_102802.jpg", "title"=>"", "filter" => "filter-varie", "x" => "1", "y" => "1");

                    shuffle($images_arr);

                
                    foreach($images_arr as $img){
                        echo '
                        
                        <!-- Grid Item -->
                        <li class="filterall '.$img['filter'].' filter-business-cards filter-posters eg-washington-copypress-wrapper" data-cobblesw="'.$img['x'].'" data-cobblesh="'.$img['y'].'">
                            <a  href="'.$img['url'].'" alt="'.$img['title'].'" data-lightbox="home-gallery">
                            <div class="esg-media-cover-wrapper">
                                <div class="esg-entry-media">
                                    <img src="'.$img['url'].'" alt="'.$img['title'].'">
                                </div>
                                <div class="esg-entry-cover esg-fade" data-delay="0">
                                    <div class="esg-overlay esg-slideright eg-washington-copypress-container" data-delay="0"></div>
                                    <div class="esg-absolute eg-washington-copypress-element-1-a esg-fade" data-delay="0.4">
                                        <!-- <a class="eg-washington-copypress-element-1" href="'.$img['url'].'" alt="'.$img['title'].'"  data-lightbox="home-gallery">
                                            <i class="eg-icon-right-open-1"></i>
                                        </a> -->
                                    </div>
                                    <div class="esg-center eg-washington-copypress-element-8 esg-none esg-clear"></div>
                                    <div class="esg-center eg-washington-copypress-element-9 esg-none esg-clear"></div>
                                    <!-- <div class="esg-bottom eg-washington-copypress-element-3 esg-slideright" data-delay="0.3">"'.$img['title'].'"</div> -->
                                </div>
                            </div>
                            </a>
                        </li>
                        <!-- /Grid Item -->
                        
                        ';
                    }


                    ?>


                    
                </ul>




            </div>
        </article>
        <div class="clear"></div>
        <div class="empty_space height_4em"></div>


<!--         <div class="sc_item_button sc_button_wrap sc_align_center">
            <a href="gallery.php" class="sc_button sc_button_pink sc_button_size_normal sc_button_icon_left">
                <span class="sc_button_text">
                    <span class="sc_button_title">Vedi tutti i progetti</span>
                </span>
            </a>
        </div> -->


        <div class="empty_space height_9_4em"></div>
    </div>
</div>
                              