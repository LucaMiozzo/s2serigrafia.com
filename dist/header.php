    <head>
        <title>S2 Serigrafia</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">

        <link rel="stylesheet" href="js/vendor/essential-grid/css/settings.css" type="text/css" media="all" />
        <link rel="stylesheet" href="css/essential-grid.css" type="text/css" media="all" />

        <link rel="stylesheet" href="js/vendor/revslider/css/settings.css" type="text/css" media="all" />
        <link rel="stylesheet" href="css/revslider.css" type="text/css" media="all" />

        <link rel="stylesheet" href="css/font-icons/css/trx_addons_icons-embedded.css" type="text/css" media="all" />
        <link rel="stylesheet" href="js/vendor/swiper/swiper.min.css" type="text/css" media="all" />
        <!-- <link rel="stylesheet" href="js/vendor/magnific/magnific-popup.min.css" type="text/css" media="all" /> -->
        <link rel="stylesheet" href="css/trx_addons_full.css" type="text/css" media="all" />
        <link rel="stylesheet" href="css/trx_addons.animation.css" type="text/css" media="all" />

        <link rel="stylesheet" href="css/font-face/Montserrat/stylesheet.css" type="text/css" media="all" />
        <link rel="stylesheet" href="css/font-face/Sofia-Pro-Light/stylesheet.css" type="text/css" media="all" />
        <link rel="stylesheet" href="css/font-face/Gilroy/stylesheet.css" type="text/css" media="all" />
        <link rel="stylesheet" href="css/font-face/PermanentMarker/stylesheet.css" type="text/css" media="all" />

        <link rel="stylesheet" href="css/fontello/css/fontello-embedded.css" type="text/css" media="all" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
        <link rel="stylesheet" href="css/colors.css" type="text/css" media="all" />
        <link rel="stylesheet" href="css/general.css" type="text/css" media="all" />
        <link rel="stylesheet" href="css/responsive.css" type="text/css" media="all" />
        <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" type="text/css" media="all" />

        <!-- Bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="all" />

        <!-- Lightbox -->
        <link href="lightbox/lightbox2/dist/css/lightbox.css" rel="stylesheet">

        <!-- custom -->
        <link rel="stylesheet" href="css/custom.css" type="text/css" media="all" />


    </head>

    <body class="home page custom-background body_tag scheme_default body_style_wide blog_style_excerpt sidebar_hide expand_content remove_margins header_style_header-custom-18 header_position_default menu_style_top no_layout">
        <!-- Body wrap -->
        <div class="body_wrap">
            <!-- Page wrap -->
            <div class="page_wrap">
                <!-- Header -->
                <header class="top_panel top_panel_custom top_panel_custom_18 without_bg_image scheme_default">
                    <!-- Top panel -->
                    <div class="top_panel_container sc_layouts_row sc_layouts_row_type_compact sc_layouts_row_fixed">
                    
                       

                            <!-- Logo -->

                            <div class="sc_content sc_content_default text-center">
                                <div class="sc_content_container">
                                    <div class="sc_layouts_item text-center header_img_container">
                                        
                                            
                                            <img src="images/header_logo.png" alt="S2 PRiNT" class="header_img">
                                            
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- /Logo -->


                            <!-- Menu -->
                            <div class="sc_layouts_item">
                                <nav class="sc_layouts_menu sc_layouts_menu_default menu_hover_slide_line hide_on_mobile" data-animation-in="fadeInUpSmall" data-animation-out="fadeOutDownSmall">
                                   
                                <div class="content_wrap">
                                    <div class="row">
                                        <ul id="menu-main-menu" class="sc_layouts_menu_nav">
                                            <!-- Menu item : Home -->
                                            <li class="menu-item current-menu-ancestor current-menu-parent col-md-3 col-sm-3 col-xs-3">
                                                <a href="index.php" class="<?php if($page == 'home') echo 'menu-item-active'; ?>">
                                                    <span>Home</span>
                                                </a>
                                            </li>
                                            <!-- /Menu item : Home -->

                                            <!-- Menu item : Services -->
                                            <li class="menu-item col-md-3 col-sm-3 col-xs-3">
                                                <a href="servizi.php" class="<?php if($page == 'services') echo 'menu-item-active'; ?>">
                                                    <span>Servizi</span>
                                                </a>
                                            </li>
                                            <!-- /Menu item : Services -->
                                            <!-- Menu item : Gallery -->
                                            <li class="menu-item col-md-3 col-sm-3 col-xs-3">
                                                <a href="#" class="<?php if($page == 'gallery') echo 'menu-item-active'; ?>">
                                                    <span>Galleria</span>
                                                </a>

                                                <ul class="sub-menu">
                                                    <li class="menu-item">
                                                        <a href="automezzi.php" class="<?php if($sub_page == 'automezzi') echo 'menu-item-active'; ?>">
                                                            <span>AUTOMEZZI</span>
                                                        </a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="vetrofanie.php" class="<?php if($sub_page == 'vetrofanie') echo 'menu-item-active'; ?>">
                                                            <span>VETROFANIE</span>
                                                        </a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="grande_formato.php" class="<?php if($sub_page == 'grande_formato') echo 'menu-item-active'; ?>">
                                                            <span>GRANDE FORMATO</span>
                                                        </a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="altro.php" class="<?php if($sub_page == 'altro') echo 'menu-item-active'; ?>">
                                                            <span>ALTRO</span>
                                                        </a>
                                                    </li>
                                                </ul>

                                            </li>
                                            <!-- /Menu item : Gallery -->
        
                                            <!-- Menu item : Contacts -->
                                            <li class="menu-item col-md-3 col-sm-3 col-xs-3">
                                                <a href="contatti.php" class="<?php if($page == 'contacts') echo 'menu-item-active'; ?>">
                                                    <span>Contatti</span>
                                                </a>
                                            </li>
                                            <!-- Menu item : Contacts -->
                                        </ul>

                                        </div>
                                    </div>
                                </nav>


                                <div class="sc_layouts_iconed_text sc_layouts_menu_mobile_button">
                                    <a class="sc_layouts_item_link sc_layouts_iconed_text_link" href="#">
                                        <span class="sc_layouts_item_icon sc_layouts_iconed_text_icon trx_addons_icon-menu">Menu</span>
                                    </a>
                                </div>
                                
                            </div>
                            <!-- /Menu -->


                        
                    </div>
                    <!-- /Top panel -->
                </header>
                <!-- /Header -->

                
                <!-- Menu mobile -->
                <div class="menu_mobile_overlay"></div>
                <div class="menu_mobile menu_mobile_fullscreen scheme_dark">
                    <div class="menu_mobile_inner">
                        <a class="menu_mobile_close icon-cancel"></a>
                        <nav class="menu_mobile_nav_area">
                            <ul id="menu_mobile-main-menu" class="">
                                <!-- Menu item : Home -->
                                <li class="menu-item current-menu-ancestor current-menu-parent">
                                    <a href="index.php">
                                        <span>Home</span>
                                    </a>
                                </li>
                                <!-- /Menu item : Home -->
                                <!-- Menu item : Services -->
                                <li class="menu-item">
                                    <a href="servizi.php">
                                        <span>Servizi</span>
                                    </a>
                                </li>
                                <!-- /Menu item : Services -->

                                <!-- Menu item : Gallery -->
                                <li class="menu-item menu-item-has-children">
                                    <a href="#">
                                        <span>Galleria</span>
                                    </a>

                                    <ul class="sub-menu">
                                        <li class="menu-item">
                                            <a href="automezzi.php">
                                                <span>AUTOMEZZI</span>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="vetrofanie.php">
                                                <span>VETROFANIE</span>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="grande_formato.php">
                                                <span>GRANDE FORMATO</span>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="altro.php">
                                                <span>ALTRO</span>
                                            </a>
                                        </li>
                                    </ul>


                                </li>
                                <!-- /Menu item : Gallery -->
                                <!-- Menu item : Contacts -->
                                <li class="menu-item">
                                    <a href="contatti.php">
                                        <span>Contatti</span>
                                    </a>
                                </li>
                                <!-- /Menu item : Contacts -->



                            </ul>
                        </nav>
                        <!-- Search -->
<!--                         <div class="search_wrap search_style_normal search_mobile">
                            <div class="search_form_wrap">
                                <form role="search" method="get" class="search_form" action="#">
                                    <input type="text" class="search_field" placeholder="Search" value="" name="s">
                                    <button type="submit" class="search_submit trx_addons_icon-search"></button>
                                </form>
                            </div>
                        </div> -->
                        <!-- /Search -->
                        <!-- Socials -->
<!--                         <div class="socials_mobile">
                            <span class="social_item">
                                <a href="https://www.facebook.com/S2-PRiNT-275996675761337/" target="_blank" class="social_icons social_facebook">
                                    <span class="trx_addons_icon-facebook"></span>
                                </a>
                            </span><span class="social_item">
                                <a href="mailto:info@s2serigrafia.com" target="_blank" class="social_icons social_facebook">
                                    <span class="trx_addons_icon-mail"></span>
                                </a>
                            </span><span class="social_item">
                                <a href="phone:+390461604288" target="_blank" class="social_icons social_gplus">
                                    <span class="trx_addons_icon-telephone"></span>
                                </a>
                            </span><span class="social_item">
                                <a href="https://www.google.it/maps/dir//S2+Serigrafia,+Via+del+Teroldego,+1,+38016+Mezzocorona+TN/@46.2062888,11.1260183,17.11z/data=!4m8!4m7!1m0!1m5!1m1!1s0x47827c824b63f2fd:0x61892f7e68f8680d!2m2!1d11.1249576!2d46.2070997" target="_blank" class="social_icons social_tumblr">
                                    <span class="trx_addons_icon-location-outline"></span>
                                </a>
                            </span>
                        </div> -->
                        <!-- /Socials -->
                    </div>
                </div>
                <!-- /Menu mobile -->