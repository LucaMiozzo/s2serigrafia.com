                <!-- Footer -->
                <footer class="footer_wrap footer_custom scheme_dark">

                    <div class="sc_content sc_content_default sc_content_width_1_1">
                        <div class="sc_content_container">


                            <div class="row">                      

                            <div class="col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0">     


                                <div class="col-md-3 col-sm-3 col-xs-4 ">

                                    <div class="footer_item"><img  class="img_icon" src="images/icons/location_icon.png" ></div> 
                                    <div class="footer_item footer_text"><span class="icon_text"><p>Via del Teroldego 1/P <br> 38016 - Mezzocorona (TN)</p></span></div>
                                </div>



  


                                <div class="col-md-3 col-sm-3 col-xs-4">
                                    <div class="footer_item"><img  class="img_icon" src="images/icons/phone_icon.png" ></div>        
                                    <div class="footer_item footer_text">
                                        <a href="phone:+390461604288" class="icon_text sc_layouts_item_link sc_layouts_iconed_text_link">                                
                                            0461 604288
                                        </a>
                                    </div>
                                </div>

                                
                                <div class="col-md-3 col-sm-3 col-xs-4"> 
                                
                                    <div class="footer_item"><img  class="img_icon" src="images/icons/mail_icon.png" ></div>  
                                    <div class="footer_item footer_text">
                                        <a href="mailto:info@s2serigrafia.com" class="icon_text sc_layouts_item_link sc_layouts_iconed_text_link">                                  
                                                info@s2serigrafia.com     
                                        </a>
                                    </div>
                                </div>                               
                                
                                <div class="col-md-3 col-sm-3 hidden-xs ">                                
                                    <a href="http://strikeweb.it" rel="nofollow" class="strikelogo col-md-6">
                                        <span class="strikeweb-container">
                                            <img src="images/strikeweb.png" alt="strikeweb" title="strikeweb"/>
                                            <span class="strikeweb">StrikeWeb</span>
                                        </span>               
                                    </a>
                                </div>
            </div>



                                                        
                                    
                            </div>   <!-- end row -->
                           

                        </div>



                    </div>


                    <div class="footer-info"> S2 PRiNT s.n.c -
                        P.IVA 01112320229        
                        <a href="#">Pricacy Policy</a>
                        <a href="#">Cookie Policy</a> 
                        

                       
                    </div>


                </footer>
                <!-- /Footer -->
            </div>
            <!-- /Page wrap -->
        </div>
        <!-- /Body wrap -->

        <script type="text/javascript" src="js/jquery/jquery.js"></script>
        <script type="text/javascript" src="js/jquery/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="js/jquery/ui/core.min.js"></script>
        <script type="text/javascript" src="js/jquery/ui/widget.min.js"></script>
        <script type="text/javascript" src="js/jquery/ui/accordion.min.js"></script>

        <script type="text/javascript" src="js/_main.js"></script>

        <script type="text/javascript" src="js/vendor/essential-grid/js/lightbox.js"></script>
        <script type="text/javascript" src="js/vendor/essential-grid/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="js/vendor/essential-grid/js/jquery.themepunch.essential.min.js"></script>
        <script type='text/javascript' src='js/vendor/revslider/jquery.themepunch.revolution.min.js'></script>
        <script type="text/javascript" src="js/eg-projects.js"></script>

        <script type="text/javascript" src="js/vendor/revslider/revsliderextensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="js/vendor/revslider/revsliderextensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="js/vendor/revslider/revsliderextensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="js/vendor/revslider/revsliderextensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="js/revslider-homepage.js"></script>

        <script type="text/javascript" src="js/vendor/swiper/swiper.jquery.min.js"></script>
        <!-- <script type="text/javascript" src="js/vendor/magnific/jquery.magnific-popup.min.js"></script> -->

        <script type="text/javascript" src="js/trx_addons.js"></script>
        <script type="text/javascript" src="js/superfish.js"></script>

        <script type="text/javascript" src="js/scripts.js"></script>
 
        <script type='text/javascript' src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDp7e0Vy6BJv0Jb0zvmOv8PdHC1DA_xO-g"></script>

        <script type="text/javascript" src="js/eg-gallery-cobbles.js"></script>
        <script type="text/javascript" src="js/eg-gallery-masonry.js"></script>

        <!-- Bootstrap js -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>

        <!-- Lightbox -->
        <script src="lightbox/lightbox2/dist/js/lightbox.js"></script>
 
        <a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>




        <script>



            jQuery(window).scroll(function() {
                if(jQuery(window).scrollTop() + jQuery(window).height() > jQuery(document).height() - 100) {
                    jQuery('.footer-info').removeClass('footer_hide');
                }else{
                    jQuery('.footer-info').addClass('footer_hide');
                }
            });



        </script>

    </body>

</html>