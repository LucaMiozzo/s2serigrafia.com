<!DOCTYPE html>
<html lang="en-US" class="no-js scheme_default">


                <?php 
                $page = 'services';
                include('header.php'); ?>


                <!-- Breadcrumbs -->
<!--                 <div class="breadcrumbs_container sc_layouts_row sc_layouts_row_type_normal sc_layouts_hide_on_frontpage scheme_dark">
                    <div class="sc_layouts_column sc_layouts_column_align_center sc_layouts_column_icons_position_left">
                        <div class="sc_content sc_content_default">
                            <div class="sc_content_container">
                                <div class="sc_layouts_item">
                                    <div class="sc_layouts_title">
                                        <div class="sc_layouts_title_title">
                                            <h2 class="sc_layouts_title_caption location-text">Servizi</h2>
                                        </div>
                                        <div class="sc_layouts_title_breadcrumbs">
                                            <div class="breadcrumbs">
                                                <a class="breadcrumbs_item home" href="index.php">Home</a>
                                                <span class="breadcrumbs_delimiter"></span>
                                                <span class="breadcrumbs_item current">Servizi</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- /Breadcrumbs -->




                <!-- Page content wrap -->
                <div class="page_content_wrap scheme_default">
                    <!-- Content -->
                    <div class="content">
                        <article class="post_item_single">
                            <div class="post_content">





                                                            <!-- Our Contacts -->
                                                            <div class="copypress-custom-bg-2">
                                    <div class="content_wrap sc_layouts_column_icons_position_left square-service">
                                        <div class="empty_space height_9_4em"></div>
                                        <div id="sc_form_3" class="sc_form sc_form_detailed simple sc_align_default">
                                            <div class="trx_addons_columns_wrap">
                                                <div class="trx_addons_column-1_2">
                                                    <h2 class="h2-left">STAMPA</h2>
                                                    <div class="sc_form_info custom_form_info orange_section">                                           

                                                        <p>
                                                            <strong><a href="#uv_print">DIRETTA UV</a></strong> (grande f.to, dibond, forex, legno, polionda)   <br>                                                     
                                                            <strong><a href="#digital_print">DIGITALE</a></strong>  <br>(adesivi piccolo e grande f.to, oneway, canvas, vinili, backlite)
                                                        </p>
                                              
                                                    </div>
                                                </div><div class="trx_addons_column-1_2">
                                                    <h2 class="h2-right">ALTRE LAVORAZIONI</h2>
                                                    <div class="sc_form_form custom_form_info gray_section">
                                       
                                                        <p>
                                                        <strong><a href="#fresa">FRESA</a></strong>  (taglio di diversi materiali)
                                                            <br>
                                                            <strong><a href="#laminazione">LAMINAZIONE</a></strong> (lucida e opaca)
                                           <br>
                                           <strong><a href="#plotter">TAGLIO CON PLOTTER</a></strong>  (vetrofanie, adesivi prespaziati, ecc)
                                                    
                                                            </p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="arrow-box">
                                            <a class="line-arrow square down" href="#uv_print"></a>      
                                        </div>
                                        
                                        <div class="empty_space height_9_4em"></div>






                                    </div>
                                </div>
                                <!-- /Our Contacts -->




                                <!-- Services Section -->
<!--                                 <div class="copypress-custom-bg-2">
                                    <div class="content_wrap sc_layouts_column_icons_position_left">
                                        <div class="empty_space height_9_2em"></div>
                                        <div class="trx_addons_columns_wrap">
                                            <div class="custom_column_1 trx_addons_column-1_1 z-index_2 sc_layouts_column_icons_position_left">
                                                <div class="copypress-custom-bg-7">
                                                    <div class="sc_services sc_services_list custom_services_1 copypress-services-custom-block-1" data-slides-per-view="2" data-slides-min-width="200">

                                                        <div class="sc_services_columns sc_item_columns sc_item_columns_2 trx_addons_columns_wrap columns_padding_bottom" style="text-align: center;">
                                                            <div class="trx_addons_column-1_4">
                                                                <div class="sc_services_item without_content with_icon sc_services_item_featured_top">
                                                                    <a href="#" class="sc_services_item_icon  icon-technology"></a>
                                                                    <div class="sc_services_item_info">
                                                                        <div class="sc_services_item_header">
                                                                            <h6 class="sc_services_item_title">
                                                                                <p>Piano fresa 3000x2000 Kongsberg</p>
                                                                            </h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><div class="trx_addons_column-1_4">
                                                                <div class="sc_services_item without_content with_icon sc_services_item_featured_top">
                                                                    <a href="#" class="sc_services_item_icon icon-technology-1"></a>
                                                                    <div class="sc_services_item_info">
                                                                        <div class="sc_services_item_header">
                                                                            <h6 class="sc_services_item_title">
                                                                                <p>Macchina da stampa HP UV Flatbed 2500</a>
                                                                            </h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><div class="trx_addons_column-1_4">
                                                                <div class="sc_services_item without_content with_icon sc_services_item_featured_top">
                                                                    <a href="#" class="sc_services_item_icon icon-pictures"></a>
                                                                    <div class="sc_services_item_info">
                                                                        <div class="sc_services_item_header">
                                                                            <h6 class="sc_services_item_title">
                                                                                <p>Plotter mimakida 1600</p><br/>
                                                                            </h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="trx_addons_column-1_4">
                                                                <div class="sc_services_item without_content with_icon sc_services_item_featured_top">
                                                                    <a href="#" class="sc_services_item_icon icon-pictures"></a>
                                                                    <div class="sc_services_item_info">
                                                                        <div class="sc_services_item_header">
                                                                            <h6 class="sc_services_item_title">
                                                                                <p>Plotter summa da intaglio 1400</p>
                                                                            </h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="trx_addons_column-1_4">
                                                                <div class="sc_services_item without_content with_icon sc_services_item_featured_top">
                                                                    <a href="#" class="sc_services_item_icon icon-technology-1"></a>
                                                                    <div class="sc_services_item_info">
                                                                        <div class="sc_services_item_header">
                                                                            <h6 class="sc_services_item_title">
                                                                                <p>Stampa termica per etichette industriali</p>
                                                                            </h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="trx_addons_column-1_4">
                                                                <div class="sc_services_item without_content with_icon sc_services_item_featured_top">
                                                                    <a href="#" class="sc_services_item_icon icon-technology-2"></a>
                                                                    <div class="sc_services_item_info">
                                                                        <div class="sc_services_item_header">
                                                                            <h6 class="sc_services_item_title">
                                                                                <p>Macchina per laminazione</p>
                                                                            </h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                         
                                        </div>
                                        <div class="empty_space height_9_2em"></div>
                                    </div>
                                </div> -->
                                <!-- /Services Section -->






                              <!-- Services Info -->
        
    <!--                             <a href="services-digital-print.php">Stampa digitale</a>
        
                                <p>Possibilità di stampare direttamente su materiali piani fino a 45mm di spessore: legno, materiali plastici, cartone, polionda, forex ecc. </p>
                                <p>Ideale per ottenere soluzioni di interior design ricercate, adatte a qualsiasi utilizzo: allestimenti fieristici, mostre museali, fotografiche, showrooms, cartellonistica da cantiere, targhe ecc. </p>
                                <p>La stampa di immagini in alta definizione consente inoltre di ricoprire e personalizzare qualunque superficie e realizzare stampe su tela, tessuto, adesivi e canvas e molte altre idee personalizzate.</p>

                                <a href="services-decorazione-automezzi.php">Decorazione automezzi</a>
    
                                <p>La versatilità dei prodotti adesivi consente una personalizzazione degli automezzi con la possibilità di applicare loghi, indirizzi, slogan, immagini. </p>
                                <p>Per una pubblicità unica, opportunità  di ricoprire integralmente o semi-integralmente il proprio automezzo con vinile adesivo di qualità, disponibile in una vasta gamma di colori oppure in modo totalmente personale con immagini o motivi studiati insieme al cliente. </p>
    


                                <a href="services-decorazione-vetri.php">Decorazione vetri</a>
                        
                                <p>Sicuramente la tecnica che permette di sfruttare al massimo la visibilità di un esercizio commerciale, con applicazione di motivi, scritte, loghi sulle vetrine secondo le esigenze e il gusto del cliente.</p>
                                <p>Applicazione anche su vetri di pellicola adesiva effetto “sabbiato”, soluzione comoda e pratica per consentire il livello desiderato di privacy o semplicemente per ottenere un effetto elegante e discreto. </p>

                                <a href="services-segnaletica-interna-esterna.php">Segnaletica interna ed esterna</a>
                        
                                <p>Ampia offerta di materiali adatti a diverse situazioni. Bandiere di diverse forme e dimensioni, cassonetti luminosi, insegne in alluminio. </p>
                                <p>Diversificata è anche la scelta della segnaletica interna per uffici, negozi, locali; targhette in plexiglass o alluminio della misura richiesta,  espositori che variano nella forma e nelle dimensioni; totem, cornici, targhe, stand, roll up, packaging. </p>


                                <a href="services-taglio-a-fresa.php">Taglio a fresa</a>
                
                                <p>Ci permette di tagliare la maggior parte dei materiali nella forma richiesta dal cliente.</p>
                                <p>Questo consente di creare mascherature complesse, insegne, supporti e articoli personalizzati e unici. </p> -->
              



                <!-- Page content wrap -->
                <div class="page_content_wrap scheme_default copypress-custom-bg-1">
                    <div class="content_wrap">
                        <!-- Content -->
                        <div class="content" >
                            
                            <article class="post_item_single page">
                                <div class="post_content services_list">
                                    <div class="empty_space height_6_67em"></div>






                                    
                                    <div class="row">
                                        <a class="anchor" name="uv_print"></a>
                                        <div class="col-md-12 text-center"><h2>Stampa diretta UV</h2></div>
                                        <div class="col-md-6">
                                            <p>Possibilità di <strong>stampare direttamente</strong> su diversi materiali piani fino a 45 mm di spessore come: <strong>polionda, forex, canvas, telo pvc, dibond, carta, cartone e legno.</strong></p>
                                            <p>Ideale per ottenere soluzioni adatte a <strong>qualsiasi utilizzo</strong>: allestimenti fieristici, mostre fotografiche e museali, showrooms, cartellonistica da cantiere, targhe e molto altro.</p>
                                        </div>

                                        <div class="col-md-6"><img src="images/services/uv-print.jpg" alt=""></div>
                                    </div>


                                    <div class="row r_gray">
                                        <a class="anchor" name="digital_print"></a>
                                        <div class="col-md-12 text-center"><h2>Stampa a solvente</h2></div>
                                        <div class="col-md-6"><img src="images/services/solvente.jpg" alt=""></div>
                                        <div class="col-md-6">
                                            <p>Grazie a questa tecnica possiamo stampare <strong>adesivi di diverse forme e misure e materiali</strong> come l’oneway adatto all’applicazione sui vetri dei mezzi.</p>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <a class="anchor" name="fresa"></a>
                                        <div class="col-md-12 text-center"><h2>Taglio e incisione a fresa</h2></div>
                                        <div class="col-md-6">
                                        <p>Ci permette di <strong>tagliare</strong> la maggior parte dei materiali nella <strong>forma</strong> richiesta dal cliente. Questo consente di creare
                                            mascherature complesse, insegne, supporti e articoli personalizzati. Oltre al taglio possiamo anche <strong>incidere</strong> i diversi
                                            materiali creando così originali e uniche soluzioni per ogni esigenza.</p>

                                        </div>
                                        <div class="col-md-6"><img src="images/services/fresa.jpg" alt=""></div>
                                    </div>


                                    <div class="row r_gray">
                                        <a class="anchor" name="laminazione"></a>
                                        <div class="col-md-12 text-center"><h2>Laminazione</h2></div>
                                        <div class="col-md-6"><img src="images/services/laminazione.jpg" alt=""></div>
                                        <div class="col-md-6">
                                            <p>Offriamo un servizio di <strong>laminazione</strong> che permette una più duratura <strong>resistenza</strong> dei prodotti realizzati. Le due opzioni tra le quali poter scegliere sono <strong>lucida</strong> oppure <strong>opaca</strong>.</p>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <a class="anchor" name="plotter"></a>
                                        <div class="col-md-12 text-center"><h2>Taglio con plotter</h2></div>
                                        <div class="col-md-6">
                                            <p>Il <strong>plotter da taglio</strong> ci permette di realizzare vetrofanie, decorazione di mezzi, adesivi e molte altre cose.</p>
                                        </div>
                                        <div class="col-md-6"><img src="images/services/plotter_img.jpg" alt=""></div>
                                    </div>








                                    <div class="empty_space height_6_67em"></div>
                                </div>
                            </article>
                        </div>
                        <!-- /Content -->
                    </div>
                </div>
                <!-- /Page content wrap -->

                                <!-- /Services Info -->


                                <!-- Counters -->
<!--                                 <div class="copypress-custom-bg-8 scheme_dark">
                                    <div class="counters-opacity-bg">                                    
                                        <div class="content_wrap sc_layouts_column_icons_position_left">
                                            <div class="empty_space height_9_2em"></div>
                                            <div class="sc_title sc_title_default">
                                                <h6 class="sc_item_subtitle sc_title_subtitle sc_align_center sc_item_title_style_yellow_sub">ALCUNE PAROLE RIGUARDO NOI</h6>
                                                <h2 class="sc_item_title sc_title_title sc_align_center sc_item_title_style_yellow_sub">Siamo fieri del nostro lavoro</h2>
                                            </div>
                                            <div class="empty_space height_4_6em"></div>
                                            <div class="sc_skills sc_skills_counter" data-type="counter">
                                                <div class="sc_skills_columns sc_item_columns trx_addons_columns_wrap columns_padding_bottom">
                                                    <div class="sc_skills_column trx_addons_column-1_4">
                                                        <div class="sc_skills_item_wrap">
                                                            <div class="sc_skills_item">
                                                                <div class="sc_skills_icon icon-content"></div>
                                                                <div class="sc_skills_total" data-start="0" data-stop="30" data-step="8" data-max="750" data-speed="35" data-duration="83" data-ed="">0</div>
                                                            </div>
                                                            <div class="sc_skills_item_title">Anni di lavoro</div>
                                                        </div>
                                                    </div><div class="sc_skills_column trx_addons_column-1_4">
                                                        <div class="sc_skills_item_wrap">
                                                            <div class="sc_skills_item">
                                                                <div class="sc_skills_icon icon-schedule"></div>
                                                                <div class="sc_skills_total" data-start="0" data-stop="456" data-step="8" data-max="750" data-speed="27" data-duration="1539" data-ed="">0</div>
                                                            </div>
                                                            <div class="sc_skills_item_title">Ore di lavoro</div>
                                                        </div>
                                                    </div><div class="sc_skills_column trx_addons_column-1_4">
                                                        <div class="sc_skills_item_wrap">
                                                            <div class="sc_skills_item">
                                                                <div class="sc_skills_icon icon-text"></div>
                                                                <div class="sc_skills_total" data-start="0" data-stop="698" data-step="8" data-max="750" data-speed="36" data-duration="3141" data-ed="">0</div>
                                                            </div>
                                                            <div class="sc_skills_item_title">Parole stampate</div>
                                                        </div>
                                                    </div><div class="sc_skills_column trx_addons_column-1_4">
                                                        <div class="sc_skills_item_wrap">
                                                            <div class="sc_skills_item">
                                                                <div class="sc_skills_icon icon-approve"></div>
                                                                <div class="sc_skills_total" data-start="0" data-stop="34" data-step="8" data-max="750" data-speed="28" data-duration="119" data-ed="">0</div>
                                                            </div>
                                                            <div class="sc_skills_item_title">Progetti finiti</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="empty_space height_9_2em"></div>
                                            <div class="empty_space height_9_2em"></div>
                                        </div>
                                    </div>                                    
                                </div> -->
                                <!-- /Counters -->

                                <!-- Brands -->
<!--                                 <div class="copypress-custom-bg-6">
                                    <div class="content_wrap sc_layouts_column_icons_position_left">
                                        <div class="empty_space height_4em"></div>
                                        <div class="brands">
                                            <div class="gallery gallery-columns-6 gallery-size-full">
                                                <figure class="gallery-item">
                                                    <div class="gallery-icon landscape">
                                                        <a href="#">
                                                            <img src="http://placehold.it/331x118.jpg" alt="" />
                                                        </a>
                                                    </div>
                                                </figure><figure class="gallery-item">
                                                <div class="gallery-icon landscape">
                                                    <a href="#">
                                                        <img src="http://placehold.it/331x118.jpg" alt="" />
                                                    </a>
                                                </div>
                                            </figure><figure class="gallery-item">
                                                <div class="gallery-icon landscape">
                                                    <a href="#">
                                                        <img src="http://placehold.it/331x118.jpg" alt="" />
                                                    </a>
                                                </div>
                                            </figure><figure class="gallery-item">
                                                <div class="gallery-icon landscape">
                                                    <a href="#">
                                                        <img src="http://placehold.it/331x118.jpg" alt="" />
                                                    </a>
                                                </div>
                                            </figure><figure class="gallery-item">
                                                <div class="gallery-icon landscape">
                                                    <a href="#">
                                                        <img src="http://placehold.it/331x118.jpg" alt="" />
                                                    </a>
                                                </div>
                                            </figure><figure class="gallery-item">
                                                <div class="gallery-icon landscape">
                                                    <a href="#">
                                                        <img src="http://placehold.it/331x118.jpg" alt="" />
                                                    </a>
                                                </div>
                                            </figure>
                                            </div>
                                        </div>
                                        <div class="empty_space height_4em"></div>
                                    </div>
                                </div> -->
                                <!-- /Brands -->
                            </div>
                        </article>
                    </div>
                    <!-- /Content -->
                </div>
                <!-- /Page content wrap -->


                <!-- Footer -->
                <?php include('footer.php'); ?>
                <!-- /Footer -->